include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules
CC=mpicc
CFLAGS = ${PETSC_CC_INCLUDES} -I/usr/local/openmpi-1.8.1/include
FFLAGS = ${PETSC_FC_INCLUDES}
# CFLAGS=-Wall -I${PETSC_DIR}/include/ -I${PETSC_DIR}/${PETSC_ARCH}/include/
# LFLAGS=-L${PETSC_DIR}/${PETSC_ARCH}/lib/ -lpetsc -lHYPRE

MAIN=driver
OBJS=driver.o assemble_matrices.o auxilliary.o

all: $(MAIN)

%.o: %.c %.h
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

$(MAIN): $(OBJS)
	$(CC) -o $(MAIN) $(OBJS) $(LFLAGS)  $(PETSC_LIB)

.phony: cleanall
cleanall:
	rm -rf *.o $(MAIN)
