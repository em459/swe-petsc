#include "assemble_matrices.h"

/* ******************************************* *
 *
 * Matrix structure
 *
 *
 *  (1) Jacobian matrix
 *
 *      [  J_{uu}   J_{uh}  ]   [ M_{uu}     0          nu/2*D_x^T ]
 *  J = [                   ] = [ 0          M_{vv}     nu/2*D_y^T ]
 *      [  J_{hu}   J_{hh}  ]   [ nu/2*D_x   nu/2*D_y   M_{hh}     ]
 *
 *  (2) RHS action matrix
 *
 *      [  0        S_{uh}  ]   [ 0         0          -nu*D_x^T ]
 *  S = [                   ] = [ 0         0          -nu*D_y^T ]
 *      [  S_{hu}   0       ]   [ -nu*D_x   -nu*D_y    0         ]
 *
 * ******************************************* */

/* ******************************************* *
 * Assemble Jacobian matrix
 * ******************************************* */
void assemble_jacobian(PetscInt nx, PetscScalar nu, Mat* J) {  
  PetscErrorCode ierr;
  Mat subJ[4];
  /* Assemble local submatrices */
  assemble_Juu(nx,&subJ[0]);
  assemble_Juh(nx,0.5*nu,&subJ[1]);
  assemble_Jhu(nx,0.5*nu,&subJ[2]);
  assemble_Jhh(nx,&subJ[3]);
  /* Create matrix nest */
  ierr = MatCreateNest(PETSC_COMM_WORLD, 2, NULL, 2, NULL, subJ, J); CHKERRQ(ierr);
}

/* ******************************************* *
 * Assemble RHS action matrix
 * ******************************************* */
void assemble_rhs_action(PetscInt nx, PetscScalar nu, Mat* S) {  
  PetscErrorCode ierr;
  Mat subS[4];
  /* Assemble local submatrices */
  ierr = MatCreate(PETSC_COMM_WORLD,&subS[0]);
  ierr = MatSetSizes(subS[0],PETSC_DECIDE,PETSC_DECIDE,2*nx*(nx-1),2*nx*(nx-1)); CHKERRQ(ierr);
  ierr = MatSetUp(subS[0]); CHKERRQ(ierr);
  ierr = MatAssemblyBegin(subS[0], MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(subS[0], MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  assemble_Juh(nx,-nu,&subS[1]);
  assemble_Jhu(nx,-nu,&subS[2]);
  ierr = MatCreate(PETSC_COMM_WORLD,&subS[3]);
  ierr = MatSetSizes(subS[3],PETSC_DECIDE,PETSC_DECIDE,nx*nx,nx*nx); CHKERRQ(ierr);
  ierr = MatSetUp(subS[3]); CHKERRQ(ierr);
  ierr = MatAssemblyBegin(subS[3], MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(subS[3], MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  /* Create matrix nest */
  ierr = MatCreateNest(PETSC_COMM_WORLD, 2, NULL, 2, NULL, subS, S); CHKERRQ(ierr);
}

/* ******************************************* *
 * Assemble velocity mass matrix
 *
 * (J_{uu} u)_{i,j} = 1/6*( u_{i-1,j} + 4*u_{i,j} + u_{i+1,j} )
 * (J_{uu} v)_{i,j} = 1/6*( v_{i,j-1} + 4*v_{i,j} + v_{i,j+1} )
 *
 * ******************************************* */
void assemble_Juu(PetscInt nx, Mat* Juu) {
  PetscErrorCode ierr;
  PetscInt ix,iy;
  PetscInt row;
  PetscInt col[3];
  PetscScalar value[3]={2.0/3.0, 1.0/6.0, 1.0/6.0};
  ierr = MatCreate(PETSC_COMM_WORLD,Juu);
  ierr = MatSetSizes(*Juu,PETSC_DECIDE,PETSC_DECIDE,2*nx*(nx-1),2*nx*(nx-1)); CHKERRQ(ierr);
  ierr = MatSetUp(*Juu); CHKERRQ(ierr);
  /* EW velocity U */
  for (ix=0;ix<=nx-2;++ix) {
    for (iy=0;iy<=nx-1;++iy) {
      row = LIDX_U(nx,ix,iy);
      col[0] = LIDX_U(nx,ix,iy);
      col[1] = LIDX_U(nx,ix-1,iy);
      col[2] = LIDX_U(nx,ix+1,iy);
      ierr = MatSetValues(*Juu,1,&row,3,col,value,INSERT_VALUES); CHKERRQ(ierr);
    }
  }
  /* NS velocity V */
  for (ix=0;ix<=nx-1;++ix) {
    for (iy=0;iy<=nx-2;++iy) {
      row = LIDX_V(nx,ix,iy);
      col[0] = LIDX_V(nx,ix,iy);
      col[1] = LIDX_V(nx,ix,iy-1);
      col[2] = LIDX_V(nx,ix,iy+1);
      ierr = MatSetValues(*Juu,1,&row,3,col,value,INSERT_VALUES); CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(*Juu, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(*Juu, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
}

/* ******************************************* *
 * Assemble (diagonal) height deviation mass matrix
 * 
 * (J_{hh} h)_{i,j} = h_{i,j}
 *
 * ******************************************* */
void assemble_Jhh(PetscInt nx, Mat* Jhh) {
  PetscErrorCode ierr;
  PetscInt ix,iy;
  PetscInt row;
  PetscInt col;
  PetscScalar value = 1.0;
  ierr = MatCreate(PETSC_COMM_WORLD,Jhh);
  ierr = MatSetSizes(*Jhh,PETSC_DECIDE,PETSC_DECIDE,nx*nx,nx*nx); CHKERRQ(ierr);
  ierr = MatSetUp(*Jhh); CHKERRQ(ierr);
  /* EW velocity U */
  for (ix=0;ix<=nx-1;++ix) {
    for (iy=0;iy<=nx-1;++iy) {
      row = LIDX_H(nx,ix,iy);
      col = LIDX_H(nx,ix,iy);
      ierr = MatSetValues(*Jhh,1,&row,1,&col,&value,INSERT_VALUES); CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(*Jhh, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(*Jhh, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
}

/* ******************************************* *
 * Assemble scaled derivative matrix
 *  J_{hu} = alpha*[D_x, D_y]
 * 
 * (J_{hu} u)_{i,j} = alpha*(u_{i,j} - u_{i-1,j})
 * (J_{hu} v)_{i,j} = alpha*(v_{i,j} - v_{i,j-1})
 *
 * ******************************************* */
void assemble_Jhu(PetscInt nx, PetscScalar alpha, Mat* Jhu) {
  PetscErrorCode ierr;
  PetscInt ix,iy;
  PetscInt row;
  PetscInt col[2];
  PetscScalar value[2] = {alpha,-alpha};
  ierr = MatCreate(PETSC_COMM_WORLD,Jhu);
  ierr = MatSetSizes(*Jhu,PETSC_DECIDE,PETSC_DECIDE,nx*nx,2*nx*(nx-1)); CHKERRQ(ierr);
  ierr = MatSetUp(*Jhu); CHKERRQ(ierr);
  /* EW velocity U */
  for (ix=0;ix<=nx-1;++ix) {
    for (iy=0;iy<=nx-1;++iy) {
      row = LIDX_H(nx,ix,iy);
      col[0] = LIDX_U(nx,ix,iy);
      col[1] = LIDX_U(nx,ix-1,iy);
      ierr = MatSetValues(*Jhu,1,&row,2,col,value,INSERT_VALUES); CHKERRQ(ierr);
    }
  }
  for (ix=0;ix<=nx-1;++ix) {
    for (iy=0;iy<=nx-1;++iy) {
      row = LIDX_H(nx,ix,iy);
      col[0] = LIDX_V(nx,ix,iy);
      col[1] = LIDX_V(nx,ix,iy-1);
      ierr = MatSetValues(*Jhu,1,&row,2,col,value,INSERT_VALUES); CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(*Jhu, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(*Jhu, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
}

/* ******************************************* *
 * Assemble scaled adjoint derivative matrix 
 *  J_{uh} = alpha*[D_x^T, D_y^T]
 * 
 * (J_{uh} h)_{i,j} = alpha*(h_{i+1,j} - h_{i,j})
 * (J_{uh} h)_{i,j} = alpha*(h_{i,j+1} - h_{i,j})
 *
 * ******************************************* */
void assemble_Juh(PetscInt nx, PetscScalar alpha, Mat* Juh) {
  PetscErrorCode ierr;
  PetscInt ix,iy;
  PetscInt row;
  PetscInt col[2];
  PetscScalar value[2] = {-alpha,alpha};
  ierr = MatCreate(PETSC_COMM_WORLD,Juh);
  ierr = MatSetSizes(*Juh,PETSC_DECIDE,PETSC_DECIDE,2*nx*(nx-1),nx*nx); CHKERRQ(ierr);
  ierr = MatSetUp(*Juh); CHKERRQ(ierr);
  /* EW velocity U */
  for (ix=0;ix<=nx-2;++ix) {
    for (iy=0;iy<=nx-1;++iy) {
      row = LIDX_U(nx,ix,iy);
      col[0] = LIDX_H(nx,ix,iy);
      col[1] = LIDX_H(nx,ix+1,iy);
      ierr = MatSetValues(*Juh,1,&row,2,col,value,INSERT_VALUES); CHKERRQ(ierr);
    }
  }
  for (ix=0;ix<=nx-1;++ix) {
    for (iy=0;iy<=nx-2;++iy) {
      row = LIDX_V(nx,ix,iy);
      col[0] = LIDX_H(nx,ix,iy);
      col[1] = LIDX_H(nx,ix,iy+1);
      ierr = MatSetValues(*Juh,1,&row,2,col,value,INSERT_VALUES); CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(*Juh, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(*Juh, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
}
