#include "auxilliary.h"

/* ******************************************* *
 * Initialise RHS
 * ******************************************* */
void initialise_rhs(PetscInt nx, Vec phi) {
  PetscErrorCode ierr;
  PetscScalar* value;
  PetscInt* row;
  PetscInt n=nx*nx;
  PetscScalar h=1.0/nx;
  value = malloc(n*sizeof(PetscScalar));
  row = malloc(n*sizeof(PetscInt));
  PetscInt ix,iy;
  PetscScalar zero=0.0;
  PetscScalar x0=0.4;
  PetscScalar y0=0.3;
  PetscScalar r0=0.2;
  PetscScalar r;

  /* Populate values for height perturbation field */
  for (ix=0;ix<nx;++ix) {
    PetscScalar x=h*(ix+0.5);
    for (iy=0;iy<nx;++iy) {
      PetscScalar y=h*(iy+0.5);
      r = sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0));
      PetscInt i=LIDX_H(nx,ix,iy);
      if (r<r0) {
        value[i] = 1.0+cos(3.14159*r/r0);
      } else {
        value[i] = 0.0;
      }
      row[i] = 2*nx*(nx-1)+i;
    }
  }
  ierr = VecSet(phi, zero); CHKERRQ(ierr);
  ierr = VecSetValues(phi, n, row, value, INSERT_VALUES); CHKERRQ(ierr);
  ierr = VecAssemblyBegin(phi); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(phi); CHKERRQ(ierr);

  free(value);
  free(row);
}

/* ******************************************* *
 * Save height field as vtk file
 * ******************************************* */
void save_vtk(char filename[], PetscInt nx, Vec phi) {
  PetscErrorCode ierr;
  FILE* file_handle;
  file_handle = fopen(filename,"w");
  PetscInt ix,iy;
  PetscScalar h=1.0/nx;
  PetscScalar* data; /* Vector data */
  /* VTK Preamble */
  fprintf(file_handle,"# vtk DataFile Version 2.0\n");
  fprintf(file_handle,"Cell data\n");
  fprintf(file_handle,"ASCII\n");
  fprintf(file_handle,"DATASET RECTILINEAR_GRID\n");
  /* Dimensions and grid coordinates */
  fprintf(file_handle,"DIMENSIONS %d %d 1\n",nx+1,nx+1);
  fprintf(file_handle,"X_COORDINATES %d float\n",nx+1);
  for (ix=0;ix<=nx;++ix) {
    fprintf(file_handle,"%e ",ix*h);
  }
  fprintf(file_handle,"\n");
  fprintf(file_handle,"Y_COORDINATES %d float\n",nx+1);
  for (iy=0;iy<=nx;++iy) {
    fprintf(file_handle,"%e ",iy*h);
  }
  fprintf(file_handle,"\n");
  fprintf(file_handle,"Z_COORDINATES 1 float\n");
  fprintf(file_handle,"0.0\n");
  /* Cell data */
  fprintf(file_handle,"CELL_DATA %d\n",nx*nx);
  fprintf(file_handle,"SCALARS field float 1\n");
  fprintf(file_handle,"LOOKUP_TABLE default\n");
  ierr = VecGetArray(phi,&data); CHKERRQ(ierr);
  for (ix=0;ix<=nx-1;++ix) {
    for (iy=0;iy<=nx-1;++iy) {
      fprintf(file_handle,"%e\n",data[2*nx*(nx-1)+LIDX_H(nx,ix,iy)]);
    }
  }
  ierr = VecRestoreArray(phi,&data); CHKERRQ(ierr);
  fclose(file_handle);
}

/* ******************************************* *
 * Calculate average height pertubation
 * ******************************************* */
PetscScalar avg_height(PetscInt nx, Vec phi) {
  PetscErrorCode ierr;
  PetscInt i;
  PetscScalar h=1.0/nx;
  PetscScalar avg=0.0;
  PetscScalar* data; /* Vector data */
  ierr = VecGetArray(phi,&data); CHKERRQ(ierr);
  for (i=0;i<nx*nx;++i) {
    avg += data[2*nx*(nx-1)+i];
  }
  ierr = VecRestoreArray(phi,&data); CHKERRQ(ierr);
  return avg*h*h;
}
