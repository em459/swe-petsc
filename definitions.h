#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_ DEFINITIONS_H_

/* Macros for defininig linearised indices */
#define LIDX_H(nx,ix,iy) ((((ix)>=0)&&((iy)>=0)&&((ix)<=((nx)-1))&&((iy)<=((nx)-1)))?((nx)*(iy)+(ix)):-1)
#define LIDX_U(nx,ix,iy) ((((ix)>=0)&&((iy)>=0)&&((ix)<=((nx)-2))&&((iy)<=((nx)-1)))?(((nx)-1)*(iy)+(ix)):-1)
#define LIDX_V(nx,ix,iy) ((((ix)>=0)&&((iy)>=0)&&((ix)<=((nx)-1))&&((iy)<=((nx)-2)))?((nx)*(((nx)-1))+(nx)*(iy)+(ix)):-1)

#endif // DEFINITIONS_H_

