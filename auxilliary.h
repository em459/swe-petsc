#ifndef AUXILLIARY_H_
#define AUXILLIARY_H_ AUXILLIARY_H_
#include <petscvec.h>
#include <petscmat.h>
#include "definitions.h"

/* ******************************************* *
 * Initialise RHS
 * ******************************************* */
void initialise_rhs(PetscInt nx, Vec phi);

/* ******************************************* *
 * Save height field as vtk file
 * ******************************************* */
void save_vtk(char filename[], PetscInt nx, Vec phi);

/* ******************************************* *
 * Calculate average height pertubation
 * ******************************************* */
PetscScalar avg_height(PetscInt nx, Vec phi);

#endif // AUXILLIARY_H_
