static char help[] = "Solves Poisson equation";
#include <sys/stat.h>
#include <stdio.h>
#include <petscksp.h>
#include "definitions.h"
#include "auxilliary.h"
#include "assemble_matrices.h"

enum PreconditionerType {
  PreTypeNone=0,
  PreTypeJacobi=1,
  PreTypeFieldsplit=2
};

/* ************************************************** *
 * SOLVER FOR THE LINEARISED SHALLOW WATER EQUATIONS
 * ************************************************** */


/*  
 * === Continuous equations ===
 *  
 *     du/dt = -c_g*dh/dx
 *     dv/dt = -c_g*dh/dy
 *     dh/dt = -c_g*(du/dx+dv/dy)
 *
 * === Implicit time discretisation ===
 * 
 *  for k = 0,...,k_max-1 do:
 *
 *     Solve the following system for (dU,dV,dH)
 *     M_u*dU + nu/2*D_x^T*dH          = R_u = -nu*D_x^T*H^{(k)}
 *     M_v*dV + nu/2*D_y^T*dH          = R_v = -nu*D_y^T*H^{(k)}
 *     nu/2*(D_x*dU + D_y*dV) + M_h*dH = R_h = -nu*(D_x*U^{(k)} + D_y*V^{(k)})
 *
 *     U^{(k+1)} = U^{(k)} + dU
 *     V^{(k+1)} = V^{(k)} + dV
 *     H^{(k+1)} = H^{(k)} + dH
 *
 *  end for
 *
 *  or using matrix notation with state vector phi = (U,V,H), dphi = (dU,dV,dH)
 * 
 *  for k = 0,...,k_max-1 do:
 *
 *    Calculate R = S*phi
 *    Solve J*dphi = R for increment dphi
 *    Update phi = phi + dphi
 *    
 *  end for
 *
 * === Staggered Arrangement of dofs on Arakawa C-grid ===
 *
 *                       +...................+
 *                       :                   :       j ("vertical" direction)
 *                       :       h_{i,j+1}   :     ^ 
 *                       :                   :     !
 *                       :         X         :     !
 *                       :                   :      ---> i ("horizontal" direction)
 *                       :       v_{i,j}     :
 *                       :         ^         :
 *   +...................+-------- ! --------+...................+
 *   :                   !                   !                   :
 *   :                   !     CELL i,j      !                   :
 *   :         u_{i-1,j} !                   ! u_{i,j}           :
 *   :        X          ->        X         ->        X         :
 *   :                   !       h_{i,j}     !                   :
 *   :     h_{i-1,j}     !                   !       h_{i+1,j}   :
 *   :                   !         ^         !                   :
 *   +...................+-------- ! --------+...................+
 *                       :       v_{i,j-1}   :
 *                       :                   :
 *                       :                   :
 *                       :         X         :
 *                       :                   :
 *                       :      h_{i,j-1}    :
 *                       :                   :
 *                       +...................+
 *
 *  Height perturbation: h_{i,j} with i = 0...nx-1, j=0...nx-1
 *  Horizontal velocity: u_{i,j} with i = 0...nx-2, j=0...nx-1
 *  Vertical velocity: v_{i,j} with i = 0...nx-1, j=0...nx-2
 *
 */

/* ******************************************* *
 *    M A I N
 * ******************************************* */
int main(int argc, char* argv[]) {
  printf("PETSc SWE solver\n");

  /* Variable declarations */
  Vec phi, r, dphi; /* solution state, RHS for linear solve and state increment */
  Mat J; /* Jacobian matrix */
  Mat S; /* RHS action matrix */
  KSP ksp; /* Iterative solver (KSP) object */
  PC pc; /* Preconditioner */
  PetscErrorCode ierr; /* PETSc error code */
  PetscInt nx=16; /* Default problem size: 16 points in 1 direction */
  PetscScalar cg=1.0; /* Gravity wave velocity */
  PetscScalar dx=1.0/nx; /* Default grid spacing */
  PetscScalar nu=1.0; /* Courant number */
  PetscScalar dt=nu*dx/cg; /* Default time step size (Courant number = 1.0) */
  PetscScalar tmax = 1.0; /* Default end time */
  size_t kmax; /* Final time step */
  int k; /* Time step index */
  PetscInt n; /* matrix size */
  PetscInt preconditioner=2; /* preconditioner type */
  PetscMPIInt size; /* MPI size */
  PetscScalar one=1.0; /* Numerical value 1.0 */
  
  /* Initialise PETSc. Currently only sequential runs are supported */
  PetscInitialize(&argc,&argv,(char*)0,help);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  if (size != 1) SETERRQ(PETSC_COMM_WORLD,1,"This code is sequential only!");

  /* Read problem parameters from command line: 
     -nx NX number of cells in one coordinate direction
     -nu NU Courant number
     -tmax TMAX final time
     -prec [0 = None, 1 = Jacobi, 2 = Fieldsplit]
   */
  ierr = PetscOptionsGetInt(NULL,"-nx",&nx,NULL); CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,"-nu",&nu,NULL); CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,"-tmax",&tmax,NULL); CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,"-tmax",&tmax,NULL); CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,"-prec",&preconditioner,NULL); CHKERRQ(ierr);
  dx = 1.0/nx;
  dt = nu*dx/cg;
  kmax = ceil(tmax/dt);

  /* Size of global matrix.
     1. horizontal velocity (1 value per interior vertical facet): nx*(nx-1)
     2. vertical velocity (1 value per interior horizontal facet): nx*(nx-1)
     3. height perturbation field (1 value per cell): nx*nx
   */
  n = nx*nx + 2*nx*(nx-1);
  printf("nx = %d\n",nx);
  printf("dx = %f\n",dx);
  printf("dt = %f\n",dt);
  printf("cg = %f\n",cg);
  printf("nu = %f\n",nu);
  printf("Total problem size is n = %d\n",n);


  /* Create vectors and set sizes */
  ierr = VecCreate(PETSC_COMM_WORLD,&phi); CHKERRQ(ierr);
  ierr = VecSetSizes(phi,PETSC_DECIDE,n);CHKERRQ(ierr); /* RHS */
  ierr = VecSetFromOptions(phi);CHKERRQ(ierr);
  ierr = VecDuplicate(phi,&dphi);CHKERRQ(ierr); /* solution increment */
  ierr = VecDuplicate(phi,&r);CHKERRQ(ierr); /* RHS for linear solve */

  /* Assemble matrices */
  assemble_jacobian(nx,nu,&J);
  assemble_rhs_action(nx,nu,&S);

  /* Initialise RHS */
  initialise_rhs(nx,phi);

  /*  === CONSTRUCT ITERATIVE SOLVER ===
   *
   *  In general, write KSP(J,P) for an iterative solver with matrix J and
   *  preconditioner P ~ J^{-1}. 
   * 
   *  For the outer iteration we use KSP = GMRES and the matrix has the structure
   *
   *                [ J_{uu}   J_{uh} ]
   *      where J = [                 ]
   *                [ J_{hu}   J_{hh} ]
   * 
   *  is the system matrix and P is the preconditioner. To construct P, use the
   *  Schur-complement approach, i.e. write
   *
   *           [ 1   -J_{uu}^{-1}*J_{uh} ]
   *  J^{-1} = [                         ]
   *           [ 0   1                   ]
   *
   *           [ J_{uu}^{-1}  0      ]
   *         * [                     ]
   *           [ 0            S^{-1} ]
   *
   *           [ 1                     0 ]
   *         * [                         ]
   *           [ -J_{hu}*J_{uu}^{-1}   1 ]
   *
   *  where S = J_{hh} - J_{hu}*J_{uu}^{-1}*J_{uh}
   *
   *  and approximate J_{uu}^{-1} -> KSP_u(J_{uu},P_{uu})
   *                  S^{-1} -> KSP_S(\hat{S},\hat{P}_S)
   *
   *  with \hat{S} = J_{hh} - J_{hu}*KSP_u(J_{uu},P_{uu})*J_{uh}
   *
   *  We use KSP_u = preonly, i.e. only apply the preconditioner once.
   *  The velocity mass matrix J_{uu} is well conditioned, so a point-Jacobi preconditioner
   *  for P_{uu} will work fine in 2d.
   *
   *  To construct a preconditioner for, note that J_{uu}^{-1} is dense, so S can not be
   *  assembled directly.
   *  By default PETSc uses J_{hh} to build the preconditioner \hat{P}_S, but this does
   *  not work if J_{hh} = 0 (or is inefficient if J_{hh} is small).
   *
   *  Instead construct a sparse matrix S' in which J_{uu}^{-1} is approximated
   *  by the inverse of the diagonal of J_{uu}, i.e.
   *
   *    S' = J_{hh} - J_{hu}*diag(J_{uu})^{-1}*J_{uh}
   *
   *  and use one AMG V-cycle with this matrix as a preconditioner.
   */

  /* Create linear solver object*/
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,J,J); CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);
  ierr = KSPSetType(ksp,KSPGMRES); CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
  if (preconditioner == PreTypeNone) {
    /* No preconditioner */
    printf("preconditioner: NONE\n");
    ierr = PCSetType(pc,PCNONE); CHKERRQ(ierr);
  }
  if (preconditioner == PreTypeJacobi) {
    /* Jacobi preconditioner */
    printf("preconditioner: JACOBI\n");
    ierr = PCSetType(pc,PCJACOBI); CHKERRQ(ierr);
  }
  if (preconditioner == PreTypeFieldsplit) {
    /* Set up fieldsplit (Schur complement) preconditioner via index sets */
    printf("preconditioner: FIELDSPLIT\n");
    ierr = PCSetType(pc,PCFIELDSPLIT); CHKERRQ(ierr);
    ierr = PCFieldSplitSetType(pc,PC_COMPOSITE_SCHUR); CHKERRQ(ierr);
    IS is[2]; /* Index sets for subspaces is[0] = velocity, is[1] = height perturbation */
    ierr = MatNestGetISs(J, is, NULL);CHKERRQ(ierr);
    ierr = PCFieldSplitSetIS(pc,"u",is[0]); CHKERRQ(ierr);
    ierr = PCFieldSplitSetIS(pc,"h",is[1]); CHKERRQ(ierr);
    /* Use J_{hh} - J_{hu}*(diag(J_{hh}))^{-1}*J_{uh} in the schur complement preconditioner instead of the default J_{hh} */
    ierr = PCFieldSplitSetSchurPre(pc,PC_FIELDSPLIT_SCHUR_PRE_SELFP,NULL); CHKERRQ(ierr);
    ierr = PCSetUp(pc); CHKERRQ(ierr);
    ierr = KSPSetUp(ksp); CHKERRQ(ierr);
    /* Extract the KSPs for velocity and height and set up their preconditioners */
    PetscInt nsplit;
    KSP* subksp;
    PC subpc_velocity, subpc_height;
    ierr = PCFieldSplitGetSubKSP(pc, &nsplit, &subksp);CHKERRQ(ierr);
    /* Set up velocity space preconditioner */
    ierr = KSPSetType(subksp[0],KSPPREONLY);
    ierr = KSPGetPC(subksp[0],&subpc_velocity); CHKERRQ(ierr);
    ierr = PCSetType(subpc_velocity,PCJACOBI); CHKERRQ(ierr);
    ierr = PCSetUp(subpc_velocity); CHKERRQ(ierr);
    /* Set up height space preconditioner */
    ierr = KSPSetType(subksp[1],KSPPREONLY);
    ierr = KSPGetPC(subksp[1],&subpc_height); CHKERRQ(ierr);
    ierr = PCSetType(subpc_height,PCHYPRE); CHKERRQ(ierr);
    ierr = PCHYPRESetType(subpc_height,"boomeramg"); CHKERRQ(ierr);
    ierr = PCSetUp(subpc_height); CHKERRQ(ierr);
  }
  /* Show information on full solver object */
  ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  /* === MAIN TIME STEPPING LOOP === */

  /* check if directory exists and create it otherwise */
  struct stat st={0};
  if (stat("./data/",&st) == -1) {
    mkdir("./data/",0700);
  }
  char filename[256];
  sprintf(filename,"data/phi_%05d.vtk",0);
  save_vtk(filename,nx,phi);
  printf("Initial avg height = %e\n",avg_height(nx,phi));
  for (k=0;k<kmax;++k) {
    /* Calculate RHS for linear solve r = S*phi^{(k)} */
    ierr = MatMult(S,phi,r);CHKERRQ(ierr);
    /* Solve linear system J*dphi = r to obtain increment dphi */
    ierr = KSPSolve(ksp,r,dphi);CHKERRQ(ierr);
    int its;
    ierr = KSPGetIterationNumber(ksp,&its);
    /* Add implicit increment to current solution */
    ierr=VecAXPY(phi,one,dphi);CHKERRQ(ierr);
    sprintf(filename,"data/phi_%05d.vtk",k+1);
    save_vtk(filename,nx,phi);
    printf("Step %6d t = %f avg height = %e #its = %d\n",k,k*dt,avg_height(nx,phi),its);
  }

  /* Deallocate memory and finalise PETSc */
  ierr = VecDestroy(&phi); CHKERRQ(ierr);
  ierr = VecDestroy(&dphi); CHKERRQ(ierr);
  ierr = VecDestroy(&r); CHKERRQ(ierr);
  ierr = MatDestroy(&J); CHKERRQ(ierr);
  ierr = MatDestroy(&S); CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp); CHKERRQ(ierr);
  ierr = PetscFinalize();
  return 0;
}
