#ifndef ASSEMBLE_MATRICES_H_
#define ASSEMBLE_MATRICES_H_ ASSEMBLE_MATRICES_H

#include <petscksp.h>
#include "definitions.h"

/* ******************************************* *
 * Jacobian matrix assembly
 * ******************************************* */
void assemble_jacobian(PetscInt nx, PetscScalar nu, Mat* J);

/* ******************************************* *
 * RHS action matrix assembly
 * ******************************************* */
void assemble_rhs_action(PetscInt nx, PetscScalar nu, Mat* S);

/* ******************************************* *
 * Assemble velocity mass matrix
 * ******************************************* */
void assemble_Juu(PetscInt nx, Mat* Juu);

/* ******************************************* *
 * Assemble (diagonal) height deviation mass matrix
 * ******************************************* */
void assemble_Jhh(PetscInt nx, Mat* Juu);

/* ******************************************* *
 * Assemble derivative matrix
 * ******************************************* */
void assemble_Jhu(PetscInt nx, PetscScalar nu, Mat* Jhu);

/* ******************************************* *
 * Assemble adjoint derivative matrix
 * ******************************************* */
void assemble_Juh(PetscInt nx, PetscScalar nu, Mat* Juh);

#endif // ASSEMBLE_MATRICES_H_
